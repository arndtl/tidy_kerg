Tidy Kerg
=========

This repository contains R code to turn the official federal election returns 2017 provided by the federal returning officer into tidy data format.

The file is called 'kerg.csv', hence the name of this repository.

It also includes the so called "Strukturdaten" (socioeconomic variables for the electoral districts and states) in a seperate file.

Files
-----

```
- tidy_kerg.csv (the electoral turns in tidy data format)
- tidy_kerg.R (code to pull the data and transform it)
- tidy_kerg.RData (the electoral turns in tidy data format)
```

Dataset
-------

One observation is the result ("Erststimme" or "Zweitstimme") for one party, in one geographic entity (electoral district, state, or national) in one year (2017 or 2013).

There are 299 electoral districts, 16 states and one federal level.

Variables
---------

```
nr -- number of electoral district, NA for states and "Bundesgebiet"
gebiet -- name of the area (electoral district, state or "Bundesgebiet")
gehoertzu -- numerical identifier for states, NA for "Bundesgebiet"
land -- name of state, NA for "Bundesgebiet"
party -- name of party
partygroup -- abbreviations for major parties, CDU and CDU named CDU/CSU, all other parties named "Sonstige"
vote -- "Erststimme" or "Zweitstimme"
period -- 2017 or 2013
votes -- number of votes cast for that party
voteshare -- voteshare for party
voters -- total number of voters in geographic entity
turnout -- turnout in geographic entity
eligible -- number of eligible citizens in geographic entity
valid -- number of valid votes in geographic entity
invalid -- number of invalid votes in geographic entity
```

Subsetting
----------

See script